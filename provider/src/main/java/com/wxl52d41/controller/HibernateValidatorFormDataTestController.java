package com.wxl52d41.controller;


import com.wxl52d41.validator.InsertValidator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.wxl52d41.vo.*;
import javax.validation.constraints.*;


/**
 * HibernateValidator参数校验测试
 */
@RestController
@RequestMapping
@Validated
public class HibernateValidatorFormDataTestController {


    /**--------------------单参数 PathVariable------------------------*/
    /**
     * 以表参格式使用 @Min @Max @NotBlank @Email 注解时需在类上添加 @Validated校验才能生效
     *
     * @param
     * @return
     * @author xlwang55
     */
    @GetMapping("/{num}")
    public Integer detail(@PathVariable("num") @Min(1) @Max(20) Integer num,
                          @RequestParam(name = "email") @NotBlank @Email String email) {
        return num * num;
    }

    /**
     * --------------------单参数 RequestParam------------------------
     */
    @GetMapping("/getByEmail")
    public TestUserVo getByAccount(@RequestParam(name = "email") @NotBlank @Email String email,
                                   @RequestParam @NotNull Integer id) {

        TestUserVo testDTO = new TestUserVo();
        testDTO.setEmail(email);
        return testDTO;
    }

    @PostMapping("/test-validation")
    public void testValidation(@RequestBody @Validated TestVO testDTO) {
        System.out.println("testDTO = " + testDTO);
    }

    @PostMapping("/testUser")
    public void testUser(@RequestBody @Validated(InsertValidator.class) User testDTO) {
        System.out.println("testDTO = " + testDTO);
    }


}
