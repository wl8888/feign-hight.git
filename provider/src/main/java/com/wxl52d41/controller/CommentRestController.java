package com.wxl52d41.controller;

import com.wxl52d41.exception.BusinessException;
import com.wxl52d41.result.Result;
import com.wxl52d41.result.ResultEnum;
import com.wxl52d41.validator.ResponseNotIntercept;
import com.wxl52d41.vo.TestVO;
import org.springframework.web.bind.annotation.*;

/**
 * 用来测试统一返回的功能
 */
@RestController
@RequestMapping("/CommentRest")
public class CommentRestController {

    /**
     * 数值返回值测试，是否能正常封装返回体
     */
    @GetMapping("getId")
    public Integer getId() {
        return 1;
    }


    @GetMapping("getTest")
    public Result<TestVO> getTest() {
        TestVO testVO = new TestVO("1", "测试标题", "无内容", "小明");
        return Result.success(testVO);
    }


    /**
     * 对象返回值测试，是否能正常封装返回体
     */
    @GetMapping("getOne")
    public TestVO getOne() {
        TestVO testVO = new TestVO("1", "测试标题", "无内容", "小明");
        if (true) {
            throw new BusinessException(ResultEnum.VALIDATE_FAILED.getCode(), "名称为空");
        }
        return testVO;
    }

    /**
     * 字符串返回值测试
     */
    @DeleteMapping("delete")
    public String delete() {
        return "删除成功";
    }

    /**
     * 无返回值测试
     *
     * @ResponseNotIntercept 使用此注解不会进行Result结果封装
     */
    @PutMapping("save")
    @ResponseNotIntercept
    public void save() {
        System.out.println("无返回值 = ");
    }
}
