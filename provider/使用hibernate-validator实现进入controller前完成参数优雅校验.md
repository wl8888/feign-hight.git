﻿
@[TOC](使用hibernate-validator实现进入controller前完成参数优雅校验)
# 一、为什么要使用validator?


参数校验在开发过程中是非常重要的一步，如果说前端的参数校验是为了用户的体验，那么后端的参数校验则为了安全。
小伙伴们有没有做过参数校验的土方法，一个一个字段去判断。这种方式弊端显而易见----代码冗余，我之前就这么干过。后来通过学习发现有现成的validator工具为什么不直接使用呢？



# 二、hibernate-validator使用步骤
   
[项目完整源代码](https://gitee.com/wl8888/controller.git)
## 2.1 定义controller全局异常处理器
使用==validator==进行参数校验时，会对不符合条件的参数进行异常抛出。
如果不加以处理异常会一直抛到前端掉用处。为了优雅的处理异常，
我们需要定义全局controller异常处理器。
CommonExceptionHandler 

```java
/**
 * 统一拦截异常
 *
 * @author xlwang
 */
@RestControllerAdvice
@Slf4j
public class CommonExceptionHandler {

    /**
     * 捕获 自定 异常
     */
    @ExceptionHandler({BusinessException.class})
    public Result<?> handleBusinessException(BusinessException ex) {
        log.error(ex.getMessage(), ex);
        return Result.failed(ex.getMessage());
    }

    /**
     * 参数缺失异常
     * 说明：参数为必填时，若入参中无此参数则会报MissingServletRequestParameterException
     */
    @ExceptionHandler({MissingServletRequestParameterException.class})
    public Result<?> handleMissingServletRequestParameterException(MissingServletRequestParameterException ex) {
        log.error(ex.getMessage(), ex);
        return Result.failed(ResultEnum.VALIDATE_FAILED.getCode(), ex.getMessage());
    }

    /**
     * 参数值校验异常
     * {@code @PathVariable} 和 {@code @RequestParam} 参数值校验不通过时抛出的异常处理
     */
    @ExceptionHandler({ConstraintViolationException.class})
    public Result<?> handleConstraintViolationException(ConstraintViolationException ex) {
        log.error(ex.getMessage(), ex);
        return Result.failed(ResultEnum.VALIDATE_FAILED.getCode(), ex.getMessage());
    }

    /**
     * 参数值类型异常
     * 说明: 定义Integer类型，输入的为String，会出现 MethodArgumentTypeMismatchException异常
     */
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public Result<?> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex) {
        log.error(ex.getMessage(), ex);
        String message = "参数:" + ex.getName() + " 类型错误";
        return Result.failed(ResultEnum.VALIDATE_FAILED.getCode(), message);
    }

    /**
     * {@code @RequestBody} 参数校验不通过时抛出的异常处理
     */
    @ExceptionHandler({MethodArgumentNotValidException.class})
    public Result<?> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        log.error(ex.getMessage(), ex);
        String msg = ex.getBindingResult().getFieldErrors().stream()
                .map(f -> f == null ? "null" : f.getField() + ": " + f.getDefaultMessage())
                .collect(Collectors.joining(", "));
        return Result.failed(ResultEnum.VALIDATE_FAILED.getCode(), msg);
    }

    @ExceptionHandler(BindException.class)
    public Result<?> handleBindException(BindException ex) {
        log.error(ex.getMessage(), ex);
        return Result.failed(HttpStatus.BAD_REQUEST.value(),
                ex.getAllErrors().stream()
                        .map(ObjectError::getDefaultMessage)
                        .collect(Collectors.joining("; "))
        );
    }

    /**
     * 捕获 {@code ForbiddenException} 异常
     */
    @ExceptionHandler({ForbiddenException.class})
    public Result<?> handleForbiddenException(ForbiddenException ex) {
        log.error(ex.getMessage(), ex);
        return Result.failed(ResultEnum.FORBIDDEN);
    }


    /**
     * 顶级异常捕获并统一处理，当其他异常无法处理时候选择使用
     */
    @ExceptionHandler({Exception.class})
    public Result<?> handle(Exception ex) {
        log.error(ex.getMessage(), ex);
        return Result.failed(ResultEnum.COMMON_FAILED);
    }

    /**
     * 处理已知的系统异常
     */
    @ExceptionHandler({ServletException.class})
    public Result<?> handle1(Exception ex) {
        log.error(ex.getMessage(), ex);
        return Result.failed(ex.getMessage());
    }

}

```
Result

```java
/**
 * 统一返回数据结构
 *
 * @author xlwang
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result<T> {
    private Integer status;
    private String message;
    private T data;

    public static <T> Result<T> success(T data) {
        return new Result<>(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getMessage(), data);
    }

    public static <T> Result<T> success(String message, T data) {
        return new Result<>(ResultEnum.SUCCESS.getCode(), message, data);
    }

    public static Result<?> failed() {
        return new Result<>(ResultEnum.COMMON_FAILED.getCode(), ResultEnum.COMMON_FAILED.getMessage(), null);
    }

    public static Result<?> failed(String message) {
        return new Result<>(ResultEnum.COMMON_FAILED.getCode(), message, null);
    }

    public static Result<?> failed(IResult errorResult) {
        return new Result<>(errorResult.getCode(), errorResult.getMessage(), null);
    }

    public static Result<?> failed(Integer code, String message) {
        return new Result<>(code, message, null);
    }

    public static <T> Result<T> instance(Integer code, String message, T data) {
        Result<T> result = new Result<>();
        result.setStatus(code);
        result.setMessage(message);
        result.setData(data);
        return result;
    }
}

```

## 2.2 测试

### 2.2.1入参为formdata表单格式验证
 以表参格式使用 @Min @Max @NotBlank @Email 注解时需在类上添加 @Validated校验才能生效
 

#### 2.2.1.1 controller
```java
@RestController
@RequestMapping
@Validated
public class HibernateValidatorFormDataTestController {

    /**--------------------单参数 PathVariable------------------------*/
    /**
     * 以表参格式使用 @Min @Max @NotBlank @Email 注解时需在类上添加 @Validated校验才能生效
     *
     * @author xlwang55
     */
    @GetMapping("/{num}")
    public Integer detail(@PathVariable("num") @Min(1) @Max(20) Integer num,
                          @RequestParam(name = "email") @NotBlank @Email String email) {
        return num * num;
    }

    /**
     * --------------------单参数 RequestParam------------------------
     */
    @GetMapping("/getByEmail")
    public TestUserVo getByAccount(@RequestParam(name = "email") @NotBlank @Email String email,
                                   @RequestParam @NotBlank String id) {
        TestUserVo testDTO = new TestUserVo();
        testDTO.setEmail(email);
        return testDTO;
    }
}
```
#### 2.2.1.2 测试结果
##### 测试detail

```java
   @GetMapping("/{num}")
    public Integer detail(@PathVariable("num") @Min(1) @Max(20) Integer num,
                          @RequestParam(name = "email") @NotBlank @Email String email) {
        return num * num;
    }
```


##### **情况一**：==num值类型错误==
当输入的参数类型和定义的不一致时会抛出`MethodArgumentTypeMismatchException`异常被定义的`handleMethodArgumentTypeMismatchException`捕获处理![在这里插入图片描述](https://img-blog.csdnimg.cn/11c08f637d2b45c7994e9c1dc07b24b4.png)
 1. 访问接口

![在这里插入图片描述](https://img-blog.csdnimg.cn/002c02054c934871a8a8a5ff1092d622.png)

 2. 控制台异常日志

```java
org.springframework.web.method.annotation.MethodArgumentTypeMismatchException: Failed to convert value of type 'java.lang.String' to required type 'java.lang.Integer'; nested exception is java.lang.NumberFormatException: For input string: ""tyu""
	
Caused by: java.lang.NumberFormatException: For input string: ""tyu""
```

##### **情况二：** ==email参数不传==
使用@RequestParam注解时默认参数不为空，当入参中缺少该参数时会抛出`MissingServletRequestParameterException`异常被定义的`handleMethodArgumentTypeMismatchException`捕获处理
![在这里插入图片描述](https://img-blog.csdnimg.cn/5ba37fcb5e9e46ddaf956eb931dfcc05.png)
 1. 访问接口
 ![在这里插入图片描述](https://img-blog.csdnimg.cn/9012055e7c0542cb839f789ae05b86f8.png)   	 
 2. 控制台异常日志
```java
org.springframework.web.bind.MissingServletRequestParameterException: Required request parameter 'email' for method parameter type String is not present
	at org.springframework.web.method.annotation.RequestParamMethodArgumentResolver.handleMissingValueInternal(RequestParamMethodArgumentResolver.java:218) ~[spring-web-5.3.22.jar:5.3.22]
	at org.springframework.web.method.annotation.RequestParamMethodArgumentResolver.handleMissingValue(RequestParamMethodArgumentResolver.java:193) ~[spring-web-5.3.22.jar:5.3.22]
	at org.springframework.web.method.annotation.AbstractNamedValueMethodArgumentResolver.resolveArgument(AbstractNamedValueMethodArgumentResolver.java:114) ~[spring-web-5.3.22.jar:5.3.22]
```

##### **情况三：** ==对参数值做校验==
在使用对参数值做些约定的注解时，例如：
使用 @NotBlank、@NotNull等被注释的元素必须不为 null时、
使用 @Min(1) @Max(20) 约定值范围时、
使用  @Email约定邮件格式时
当参数值不符合条件时会抛出`ConstraintViolationException
`异常被定义的`handleConstraintViolationException`捕获处理

 1. email参数值为空

![在这里插入图片描述](https://img-blog.csdnimg.cn/8a80d62f90824e71b39cd41c1227ea7c.png)

```java
javax.validation.ConstraintViolationException: detail.email: 不能为空
	at org.springframework.validation.beanvalidation.MethodValidationInterceptor.invoke(MethodValidationInterceptor.java:120) ~[spring-context-5.3.22.jar:5.3.22]
	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:186) ~[spring-aop-5.3.22.jar:5.3.22]	
```

 2. 邮件不合法
 ![在这里插入图片描述](https://img-blog.csdnimg.cn/246e74a167694ee084b4140848745cd6.png)
 

```java
javax.validation.ConstraintViolationException: detail.email: 不是一个合法的电子邮件地址
	at org.springframework.validation.beanvalidation.MethodValidationInterceptor.invoke(MethodValidationInterceptor.java:120) ~[spring-context-5.3.22.jar:5.3.22]
	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:186) ~[spring-aop-5.3.22.jar:5.3.22]
	
```

3. num值不在规定范围内
![在这里插入图片描述](https://img-blog.csdnimg.cn/91dadedfd84648bda7d0f977aa3381ba.png)

```java
javax.validation.ConstraintViolationException: detail.num: 最大不能超过20
	at org.springframework.validation.beanvalidation.MethodValidationInterceptor.invoke(MethodValidationInterceptor.java:120) ~[spring-context-5.3.22.jar:5.3.22]
	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:186) ~[spring-aop-5.3.22.jar:5.3.22]

```
4. 多个参数值校验不通过时
系统会将多个校验不通过参数值异常统一抛出
![在这里插入图片描述](https://img-blog.csdnimg.cn/7899fd134541480aa444790280593543.png)

### 2.2.2入参为json格式验证

#####  2.2.2.1 定义新增和更新验证器

 - `InsertValidator`：新增时对应的字段校验生效
 
 - `UpdateValidator`：更新时对应的字段校验生效
![在这里插入图片描述](https://img-blog.csdnimg.cn/db276393ca064cb5aaff5db0b945d4cc.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5LyP5Yqg54m56YGH5LiK6KW_5p-a,size_20,color_FFFFFF,t_70,g_se,x_16)
InsertValidator
```java
/**
 * 新增验证器
 *
 */
public interface InsertValidator {

}

```
   UpdateValidator
```java
/**
 * 编辑验证器
 *
 */
public interface UpdateValidator {

}

```


#####  2.2.2.2在实体类中添加校验条件
JSR 303只是个规范，并没有具体的实现，目前通常都是由hibernate-validator进行统一参数校验。

==JSR303定义的基础校验类型：==

 - `@Null`	被注释的元素必须为 null
 - `@NotNull`	被注释的元素必须不为 null
- `@AssertTrue`	被注释的元素必须为 true
- `@AssertFalse`	被注释的元素必须为 false
- `@Min(value)`	被注释的元素必须是一个数字，其值必须大于等于指定的最小值
- `@Max(value)`	被注释的元素必须是一个数字，其值必须小于等于指定的最大值
- `@DecimalMin(value)`	被注释的元素必须是一个数字，其值必须大于等于指定的最小值
- `@DecimalMax(value)`	被注释的元素必须是一个数字，其值必须小于等于指定的- 最大值
- `@Size(max, min)`	被注释的元素的大小必须在指定的范围内
- `@Digits (integer, fraction)`	被注释的元素必须是一个数字，其值必须在可接受的范围内
- `@Past`	被注释的元素必须是一个过去的日期
- `@Future`	被注释的元素必须是一个将来的日期
- `@Pattern(value)`	被注释的元素必须符合指定的正则表达式
 
==Hibernate Validator 中附加的 constraint ：==
- `@Email`	被注释的元素必须是电子邮箱地址
- `@Length`	被注释的字符串的大小必须在指定的范围内
- `@NotEmpty`	被注释的字符串的必须非空
- `@Range`	被注释的元素必须在合适的范围内
 
```java
@Data
public class User {


    @NotNull(groups = { UpdateValidator.class })
    private Long id;// ID
    /**
     * 新增时校验条件生效
     * 用户名不能为空，且用户名个数在1-20之间。
     * */
    @NotEmpty(message = "username必填",groups = { InsertValidator.class })
    @Size(max = 20, min = 1,message = "用户名个数在1-20之间", groups = { InsertValidator.class })
    private String username;// 用户名

    @NotBlank(message = "密码必填",groups = { InsertValidator.class })
    @Size(min = 6,message = "密码个数不少于六位", groups = { InsertValidator.class })
    private String password;// 密码

    /**
     * 新增时校验条件生效
     * 邮箱校验规则
     * */
    @NotBlank(message = "邮箱不能为空",groups = { InsertValidator.class })
    @Email(groups = { InsertValidator.class })
    @Size(max = 30,message = "邮箱个数不多于30位", groups = { InsertValidator.class })
    private String email;// 邮箱

    /**
     * 新增时校验条件生效
     * 使用了手机号校验规则
     * */
    @NotBlank(message = "手机号不能为空",groups = { InsertValidator.class })
    @TelephoneNumber(groups = { InsertValidator.class },message = "手机号不正确")
    @Size(max = 20, groups = { InsertValidator.class })
    private String mobile;// 手机号

    @Size(max = 20, min = 6, groups = {InsertValidator.class, UpdateValidator.class })
    private String provinceRegionCode;// 省份区域代码

    @Size(max = 20, min = 6, groups = { UpdateValidator.class })
    private String cityRegionCode;// 地市区域代码

    @Size(max = 40, groups = { UpdateValidator.class })
    private String address;// 街道地址

    @Size(max = 256, groups = { UpdateValidator.class })
    private String profile;// 个人简介

}

```

#####  2.2.2.3 在controller中添加注解
 - `@Validated(InsertValidator.class)` 所有`group`中使用`InsertValidator`的属性校验效
 - `@Validated(UpdateValidator.class)`所有`group`中使用`UpdateValidator`的属性校验生效

```java
@RestController
@RequestMapping("/user")
public class UserController {
    /**
     * 注册用户
     *
     * @param user 用户详细信息
     * @return
     */
    @PostMapping(path = "/insertUser")
    public void insertUser(@Validated(InsertValidator.class) @RequestBody User user) {
        System.out.println("user = " + user);
    }

    /**
     * 编辑用户详细信息
     *
     * @param user 用户详细信息
     * @return
     */
    @PostMapping(path = "/updateUser")
    public void updateUser(@Validated(UpdateValidator.class) @RequestBody User user) {
        System.out.println("user = " + user);
    }

    /**
     * 修改用户密码
     *
     * @param password    原密码
     * @param newPassword 新密码
     * @param id          用户ID
     * @return
     */
    @PutMapping(path = "/updatePassword")
    public void updatePassword(@RequestParam(name = "password", required = true) String password,
                               @RequestParam(name = "newPassword", required = true) String newPassword,
                               @RequestParam(name = "id", required = true) Long id) {

    }

}

```

#####  2.2.2.4 测试结果
使用`@RequestBody`传参为`json`时，对参数校验异常会抛出`MethodArgumentNotValidException`异常，
会被`handleMethodArgumentNotValidException`捕获处理![在这里插入图片描述](https://img-blog.csdnimg.cn/f88f350618a746cfb7817737d9ed9f96.png)

######  2.2.2.4.1 执行insertUser操作
 1. `username` 校验
 

```java
    /**
     *  用户名
     *  使用了 groups = {InsertValidator.class} 新增时校验条件生效
     *  message为自定义内容，可以不填，使用默认的。
     * 用户名不能为空，且用户名个数在1-20之间。
     */
    @NotEmpty(message = "username必填", groups = {InsertValidator.class})
    @Size(max = 20, min = 1, message = "用户名个数在1-20之间", groups = {InsertValidator.class})
    private String username;

```

==入参 名称为空时==

```json
{
  "id": 1,
  "username": "",
  "password": "demoData",
  "email": "1234@qq.com",
  "mobile": "18898987878",
  "provinceRegionCode": "demoData",
  "cityRegionCode": "demoData",
  "address": "demoData",
  "profile": "demoData"
}
```
==结果==
结果1：message中自定义了消息
![在这里插入图片描述](https://img-blog.csdnimg.cn/f4d417338bce4670ab99f8b0e70f79e4.png)

结果2 ：不添加自定义消息
![在这里插入图片描述](https://img-blog.csdnimg.cn/325df0c507e34db982b18dda06a3dfb7.png)


 2. `password` 校验

![在这里插入图片描述](https://img-blog.csdnimg.cn/937e1521d92c457c856c75ab90ce8cf7.png)

1.密码为空时

```json
{
  "id": 1,
  "username": "小明",
  "password": "",
  "email": "1234@qq.com",
  "mobile": "18898987878",
  "provinceRegionCode": "demoData",
  "cityRegionCode": "demoData",
  "address": "demoData",
  "profile": "demoData"
}
```
结果

```json
{
    "status": 400,
    "message": "password: 密码必填, password: 密码个数不少于六位",
    "data": null
}
```

 2.密码个数少于六位时
 

```json
{
  "id": 1,
  "username": "小明",
  "password": "123",
  "email": "1234@qq.com",
  "mobile": "18898987878",
  "provinceRegionCode": "demoData",
  "cityRegionCode": "demoData",
  "address": "demoData",
  "profile": "demoData"
}
```

 结果
 

```r
{
    "status": 400,
    "message": "password: 密码个数不少于六位",
    "data": null
}
```

 
 3. 邮箱校验
 
![邮箱校验](https://img-blog.csdnimg.cn/ea700a3c211c4e13819dd889e15a3e59.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5LyP5Yqg54m56YGH5LiK6KW_5p-a,size_20,color_FFFFFF,t_70,g_se,x_16)
1.邮箱为空时

```json
{
  "id": 1,
  "username": "小明",
  "password": "122223",
  "email": "",
  "mobile": "18898987878",
  "provinceRegionCode": "demoData",
  "cityRegionCode": "demoData",
  "address": "demoData",
  "profile": "demoData"
}
```
结果

```json
{
    "status": 400,
    "message": "email: 邮箱不能为空",
    "data": null
}
```
2.邮箱不符合规则时
```json
{
  "id": 1,
  "username": "小明",
  "password": "122223",
  "email": "1234",
  "mobile": "18898987878",
  "provinceRegionCode": "demoData",
  "cityRegionCode": "demoData",
  "address": "demoData",
  "profile": "demoData"
}
```
结果

```json
{
    "status": 400,
    "message": "email: 不是一个合法的电子邮件地址",
    "data": null
}
```

 
 4. 手机号验证
手机号属性上使用的新增和更新时验证。当在controller方法中，添加@Validated(InsertValidator.class)和@Validated(UpdateValidator.class)时都会执行校验逻辑。
使用了自定义的手机号校验注解，验证手机号正确性
![在这里插入图片描述](https://img-blog.csdnimg.cn/c7571906f0b74042a20e0b186097d0d7.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5LyP5Yqg54m56YGH5LiK6KW_5p-a,size_20,color_FFFFFF,t_70,g_se,x_16)
入参
手机号超过11位时
```json
{
  "id": 1,
  "username": "小明",
  "password": "122223",
  "email": "1234@qq.com",
  "mobile": "188989878788",
  "provinceRegionCode": "demoData",
  "cityRegionCode": "demoData",
  "address": "demoData",
  "profile": "demoData"
}
```
结果

```json
{
    "status": 400,
    "message": "mobile: 个数必须在0和11之间, mobile: 手机号不正确",
    "data": null
}
```
###### 2.2.2.4.2  执行updateUser操作
1.手机号验证
手机号属性上使用的新增和更新时验证。当在controller方法中，添加@Validated(UpdateValidator.class)时都会执行校验逻辑。
![在这里插入图片描述](https://img-blog.csdnimg.cn/60afd357399d473ea67906e0aff457e6.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5LyP5Yqg54m56YGH5LiK6KW_5p-a,size_20,color_FFFFFF,t_70,g_se,x_16)
入参
手机号超过11位时
```json
{
  "id": 1,
  "username": "demoData",
  "password": "demoData",
  "email": "demoData",
  "mobile": "188959589892",
  "provinceRegionCode": "demoData",
  "cityRegionCode": "demoData",
  "address": "demoData",
  "profile": "demoData"
}
```
结果

```json
{
    "status": 400,
    "message": "mobile: 手机号不正确, mobile: 个数必须在0和11之间",
    "data": null
}
```

## 手机号验证使用了自定义注解

![在这里插入图片描述](https://img-blog.csdnimg.cn/f2afb3f84f2048f0b4978b9211f7bb36.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5LyP5Yqg54m56YGH5LiK6KW_5p-a,size_20,color_FFFFFF,t_70,g_se,x_16)

```java
/**
 * 手机号码验证器
 *
 */
@Documented
@Constraint(validatedBy = {})
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RUNTIME)
@ReportAsSingleViolation
@Pattern(regexp = "^((13[0-9])|(15[^4])|(18[0-9])|(17[0-9])|(147))\\d{8}$")
public @interface TelephoneNumber {

	String message() default "手机号不正确";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
```

## 优秀扩展
[Controller 就该这么写](https://juejin.cn/post/7123091045071454238#heading-12)
[Spring Validation最佳实践及其实现原理，参数校验没那么简单！](https://juejin.cn/post/6856541106626363399)

