# 项目介绍

问题一：

在实际项目我们需要对入参进行数据合法校验，这一步骤若是手动在业务中校验那岂不是显得程序员很low。

问题二：

程序中会发生一些已知和未知的异常，如果我们自己写try catch那无疑增加了工作量，还似使得不清爽。可以采用统一异常处理，自动处理指定的异常和未知异常。

问题三：

我们在controller中是不是经常看见封装result返回消息体，那么能有方法自动帮我们封装吗？

**本项目针对上述问题，对controller层进行处理：**

- 数据合法性校验

- 统一异常处理

- 统一返回结果封装

## 数据合法性校验

[使用hibernate-validator实现进入controller前完成参数优雅校验](https://blog.csdn.net/weixin_43811057/article/details/123157228?spm=1001.2014.3001.5501)

## 统一异常处理

[SpringBoot项目异常的统一处理](https://blog.csdn.net/weixin_43811057/article/details/115054630)

## 日志记录

[lockback日志记录](https://blog.csdn.net/weixin_43811057/article/details/124549135)

[SpringBoot的统一日志记录](https://blog.csdn.net/weixin_43811057/article/details/115054888?spm=1001.2014.3001.5501)

## 统一返回结果封装

[SpringBoot统一封装controller层返回的结果](http://t.csdn.cn/N1Mzs)