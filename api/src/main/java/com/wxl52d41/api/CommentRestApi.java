package com.wxl52d41.api;

import com.wxl52d41.result.Result;
import com.wxl52d41.vo.TestVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;

/**
 * @ClassName: CommentRestApi
 * @Description: CommentRestApi
 * @Author: wang xiao le
 * @Date: 2023/08/25 23:32
 **/
@FeignClient(url = "http://localhost:9098", path = "CommentRest", value = "provider")
public interface CommentRestApi {
    @GetMapping("getTest")
    Result<TestVO> getTest();

    /**
     * 数值返回值测试，是否能正常封装返回体
     */
    @GetMapping("getId")
    Integer getId();

    /**
     * 对象返回值测试，是否能正常封装返回体
     */
    @GetMapping("getOne")
    TestVO getOne();

    /**
     * 字符串返回值测试
     */
    @DeleteMapping("delete")
    String delete();

    /**
     * 无返回值测试
     *
     * @ResponseNotIntercept 使用此注解不会进行Result结果封装
     */
    @PutMapping("save")
    void save();


}
