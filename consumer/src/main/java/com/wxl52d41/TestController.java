package com.wxl52d41;

import com.wxl52d41.api.CommentRestApi;
import com.wxl52d41.result.Result;
import com.wxl52d41.vo.TestVO;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @ClassName: TestController
 * @Description: TestController
 * @Author: wang xiao le
 * @Date: 2023/08/26 15:51
 **/
@RestController
@RequestMapping
public class TestController {

    @Resource
    CommentRestApi commentRestApi;

    @GetMapping("getId")
    public Integer getId() {
        return commentRestApi.getId();
    }

    @GetMapping("getOne")
    public TestVO getOne() {
        TestVO one = commentRestApi.getOne();
        System.out.println("one = " + one);
        return one;
    }

    @GetMapping("getContent")
    public Result<String> getContent() {
        String content=null;
        Result<TestVO> test = commentRestApi.getTest();
        if (test.isSuccess()) {
            TestVO data = test.getData();
             content = data.getContent();
        }else {
            throw new  RuntimeException(test.getMessage());
        }

        return Result.success(content);
    }

    /**
     * 字符串返回值测试
     */
    @DeleteMapping("delete")
    public String delete() {
        return commentRestApi.delete();
    }

}
