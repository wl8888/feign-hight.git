package com.wxl52d41;

import com.wxl52d41.api.CommentRestApi;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
class ConsumerApplicationTests {
    @Resource
    CommentRestApi commentRestApi;

    @Test
    void contextLoads() {

        System.out.println("id = " + commentRestApi.getOne());
    }

}
